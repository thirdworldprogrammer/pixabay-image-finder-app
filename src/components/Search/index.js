import React, { Component } from 'react'
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import axios from 'axios';

import ImageResults from '../ImageResults';

import styles from './style.css';

class Search extends Component {
  state = {
    searchText: '',
    amount: 20,
    apiUrl: 'https://pixabay.com/api',
    apiKey: '',
    images: []
  }

  onTextChange = (e) => {
    const val = e.target.value;
    this.setState({ [e.target.name]: e.target.value }, () => {
      if (val === '') {
        this.setState({ images: [] });
      } else {
        axios.get(`${this.state.apiUrl}/?key=${this.state.apiKey}&q=${this.state.searchText}&image_type=photo&per_page=${this.state.amount}&safeSearch=true`)
          .then((res) => {
            let ourImageArray = res.data.hits;
            if (ourImageArray) {
              this.setState({ images: ourImageArray });
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    });
  }

  onAmountChange = (e, index, value) => {
    this.setState({ amount: value });
  }
  render() {
    console.log(this.state);
    return (
      <div className={styles['search-component']}>
        <TextField
          name="searchText"
          value={this.state.searchText}
          onChange={this.onTextChange}
          floatingLabelText="Search For Images"
          fullWidth={true}
        />
        <br />

        <SelectField
          name="amount"
          floatingLabelText="Frequency"
          value={this.state.amount}
          onChange={this.onAmountChange}
        >
          <MenuItem value={5} primaryText="5" />
          <MenuItem value={10} primaryText="10" />
          <MenuItem value={20} primaryText="20" />
          <MenuItem value={40} primaryText="40" />
          <MenuItem value={80} primaryText="80" />
        </SelectField>
        <ImageResults images={this.state.images} />
      </div>
    );
  }
}

export default Search;
