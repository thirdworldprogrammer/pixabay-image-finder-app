This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

A simple Image finder app, built using react, material-ui, and makes use of the pixabay image finder app.

Feel free to use the code as you see fit. :)

Commands can be found in the package.json file
